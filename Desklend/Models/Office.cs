﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Desklend.Models
{
    public class Office
    {
        [Key]
        [JsonProperty(PropertyName = "idOffice")]
        public int IdOffice { get; set; }
        [Required]
        [JsonProperty(PropertyName = "idCompany")]
        public int IdCompany { get; set; }
        [Required]
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }
        [Required]
        [JsonProperty(PropertyName = "street")]
        public string Street { get; set; }
        [Required]
        [JsonProperty(PropertyName = "civicNumber")]
        public int CivicNumber { get; set; }
        [Required]
        [JsonProperty(PropertyName = "cap")]
        public int Cap { get; set; }
        [Required]
        [JsonProperty(PropertyName = "telefone")]
        public int Telefone { get; set; }

    }
}
