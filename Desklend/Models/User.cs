﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Desklend.Models
{
    public class User
    {
        [Key]
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [Required]
        [JsonProperty(PropertyName = "idCompany")]
        public int IdCompany { get; set; }
        [Required]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [Required]
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }
        [Required]
        [JsonProperty(PropertyName = "pw")]
        public string Password { get; set; }
        [Required]
        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }
    }
}
