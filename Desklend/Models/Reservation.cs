﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Desklend.Models
{
    public class Reservation
    {
        [Key]
        [JsonProperty(PropertyName = "idReservation")]
        public int IdReservation { get; set; }
        [Required]
        [JsonProperty(PropertyName = "idDesk")]
        public int IdDesk { get; set; }
        [Required]
        [JsonProperty(PropertyName = "idUser")]
        public int IdUser { get; set; }
        [Required]
        [JsonProperty(PropertyName = "dataOraIn")]
        public string DataOraIn { get; set; }
        [Required]
        [JsonProperty(PropertyName = "dataOraOut")]
        public string DataOraOut { get; set; }
        [Required]
        [JsonProperty(PropertyName = "confirm")]
        public bool Confirm{ get; set; }

    }
}
