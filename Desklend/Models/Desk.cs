﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Desklend.Models
{
    public class Desk
    {
        [Key]
        [JsonProperty(PropertyName = "idDesk")]
        public int IdDesk { get; set; }
        [Required]
        [JsonProperty(PropertyName = "idRoom")]
        public int IdRoom { get; set; }
        [Required]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [Required]
        [JsonProperty(PropertyName = "availability")]
        public bool Availability { get; set; }
        [Required]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [Required]
        [JsonProperty(PropertyName = "wifi")]
        public bool Wifi { get; set; }
        [Required]
        [JsonProperty(PropertyName = "lan")]
        public bool LanConnection { get; set; }
        [Required]
        [JsonProperty(PropertyName = "printer")]
        public bool Printer { get; set; }
        [Required]
        [JsonProperty(PropertyName = "electricCurrent")]
        public bool ElectricCurrent { get; set; }
        [Required]
        [JsonProperty(PropertyName = "monitor")]
        public bool Monitor { get; set; }
        [Required]
        [JsonProperty(PropertyName = "keyboard")]
        public bool Keyboard { get; set; }
        [Required]
        [JsonProperty(PropertyName = "mouse")]
        public bool Mouse { get; set; }
        [Required]
        [JsonProperty(PropertyName = "plotter")]
        public bool Plotter { get; set; }


    }
}
