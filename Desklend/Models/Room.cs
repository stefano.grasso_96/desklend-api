﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Desklend.Models
{
    public class Room
    {
        [Key]
        [JsonProperty(PropertyName = "idRoom")]
        public int IdRoom { get; set; }
        [Required]
        [JsonProperty(PropertyName = "idOffice")]
        public int IdOffice { get; set; }
        [Required]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}
