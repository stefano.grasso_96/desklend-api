﻿using Desklend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Desklend
{
    public class DesklendDBContext:DbContext
    {
        public DbSet<Company> Company { get; set; }
        public DesklendDBContext(DbContextOptions<DesklendDBContext> options):base(options)
        {

        }
        public DbSet<Desklend.Models.User> Users { get; set; }
        public DbSet<Desklend.Models.Office> Office { get; set; }
        public DbSet<Desklend.Models.Reservation> Reservation { get; set; }
        public DbSet<Desklend.Models.Room> Rooms { get; set; }
        public DbSet<Desklend.Models.Desk> Desk { get; set; }
    }
}
